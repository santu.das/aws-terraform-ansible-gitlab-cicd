resource "aws_instance" "main" {
  count          = var.instance_count
  ami            = var.ami_id
  instance_type  = var.instance_type
  subnet_id      = element(var.subnet_ids, count.index)
  key_name       = var.key_name
  security_groups = var.security_groups

  user_data = <<-EOF
  #!/bin/bash
  echo "*** Installing apache2"
  sudo apt update -y
  sudo apt install apache2 -y
  sudo systemctl start apache2
  sudo systemctl enable apache2
  echo "<center><h1>Welcome!!</h1><hr><h2>I'm <i>$(hostname -f)</i></h2></center>" > /var/www/html/index.html
  echo "*** Completed Installing apache2"
  EOF

  tags = {
    Name = "${var.environment}-${var.instance_name}-${count.index}"
  }
}