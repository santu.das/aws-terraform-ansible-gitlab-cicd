# Automating AWS Infrastructure Deployment with Terraform and Ansible

## Overview

This project automates the provisioning of AWS infrastructure using Terraform and Ansible. It includes reusable modules for network and compute resources and supports multiple environments (dev and prod). The project is integrated with GitLab CI/CD to streamline the deployment process.

## Project Structure

- `ansible/`: Ansible Playbooks for Automated Configuration Management with Terraform
- `modules/`: Contains reusable Terraform modules for network and compute resources.
  - `network/`: Manages the creation of VPCs and subnets.
  - `compute/`: Manages the creation of EC2 instances.
- `envs/`: Contains environment-specific configurations.
  - `dev/`: Development environment.
  - `prod/`: Production environment.
- `.gitlab-ci.yml`: GitLab CI configuration for automating Terraform workflows.
- `README.md`: Project documentation.

## Features

- **Modular Design**: Uses Terraform modules to promote reusability and maintainability.
- **Multi-Environment Support**: Allows separate configurations for development and production environments.
- **Automated CI/CD**: Integrates with GitLab CI/CD to automate the validation, planning, and application of Terraform configurations.

## Usage

### Prerequisites

- Ensure you have access to an AWS account.
- Store your AWS credentials as environment variables in GitLab CI/CD settings.

### Running the Pipeline

1. **Plan Changes**:
   - Manually trigger the `plan` stage, specifying the environment (`dev`, `prod`, or `both`) in the `ENVIRONMENT` variable.
2. **Apply Changes**:
   - Review the plan outputs.
   - Manually trigger the `apply` stage with the same `ENVIRONMENT` variable to deploy the changes.

### AWS Credentials

#### Store your AWS credentials as protected environment variables in GitLab CI/CD:

- `AWS_ACCESS_KEY_ID`: Your AWS Access Key ID.
- `AWS_SECRET_ACCESS_KEY`: Your AWS Secret Access Key.
- `AWS_DEFAULT_REGION`: Your preferred AWS region (e.g., `us-west-2`).

#### Steps to Store AWS Keys in GitLab CI/CD
	1. Navigate to Your Project Settings:
		- Go to your GitLab project.
		- Click on Settings > CI/CD.
	2. Set Up Environment Variables:
    	- Expand the Variables section.
    	- Click on the Add Variable button.
    	- Add the following variables:
    	- AWS_ACCESS_KEY_ID: Your AWS Access Key ID.
    	- AWS_SECRET_ACCESS_KEY: Your AWS Secret Access Key.
    	- AWS_DEFAULT_REGION: Your preferred AWS region (e.g., us-west-2).
    	- Make sure to mark these variables as protected if you want them to be available only on protected branches and tags.
    	- Optionally, mark them as masked to hide their values in job logs.

### Example Commands

1. **Initialize the Terraform backend**:
    ```
    terraform -chdir=envs/dev init
    terraform -chdir=envs/prod init
    ```

2. **Validate the configuration**:
    ```
    terraform -chdir=envs/dev validate
    terraform -chdir=envs/prod validate
    ```

3. **Plan the changes**:
    ```
    terraform -chdir=envs/dev plan -out=dev.tfplan
    terraform -chdir=envs/prod plan -out=prod.tfplan
    ```

4. **Apply the changes**:
    ```
    terraform -chdir=envs/dev apply -auto-approve dev.tfplan
    terraform -chdir=envs/prod apply -auto-approve prod.tfplan
    ```

5. **Destroy the enviroment**:
    ```
    terraform -chdir=envs/dev destroy -auto-approve
    terraform -chdir=envs/prod destroy -auto-approve
    ```

## Verify Web Application on EC2

Validate that the demo web application, running on a container and hosted on port **3000**, is operational. To confirm, follow these steps:

1. Obtain the public IP address of any EC2 instance and append port **3000** to it (e.g., **http://[ec2-public-ip]:3000**).
2. Open a web browser and navigate to the constructed URL. If the application is running, you should see the web application's user interface.

This step verifies that the Terraform Ansible provisioner has successfully deployed and configured the web application, making it accessible on the designated port, as expected.


## Contribution

Feel free to fork this repository and submit pull requests. For major changes, please open an issue first to discuss what you would like to change.

## License

This project is licensed under the LICENSE. See the LICENSE file for details.